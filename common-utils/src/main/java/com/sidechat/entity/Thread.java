package com.sidechat.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("Thread")
public class Thread implements Serializable {
    @Id
    String id;
    String url;
    String title;
    String creator;
    Date startDate;

    @NonNull
    Long lastPage;
}
