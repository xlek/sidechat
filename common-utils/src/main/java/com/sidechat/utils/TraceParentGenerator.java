package com.sidechat.utils;

import io.opentelemetry.sdk.trace.IdGenerator;

import java.util.concurrent.atomic.AtomicLong;

public class TraceParentGenerator implements IdGenerator {
    private static final AtomicLong traceId = new AtomicLong(0);
    private static final AtomicLong spanId = new AtomicLong(0);

    public String generateTraceparent() {
        return "%s-%s-%s-%s".formatted(
                "00", this.generateTraceId(), this.generateSpanId(), "01"
        );
    }

    @Override
    public String generateSpanId() {
        return String.format("%016d", spanId.incrementAndGet());
    }

    @Override
    public String generateTraceId() {
        return String.format("%032d", traceId.incrementAndGet());
    }
}
