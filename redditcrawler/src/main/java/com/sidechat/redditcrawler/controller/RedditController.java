package com.sidechat.redditcrawler.controller;

import com.sidechat.redditcrawler.service.RedditCrawlerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
public class RedditController {

    @Autowired
    private RedditCrawlerService redditCrawlerService;

    @GetMapping("/test")
    public String crawlSubReddit(@RequestParam String subreddit) {
        log.info("Crawling post from sub-reddit {}", subreddit);
        return redditCrawlerService.crawlSubreddit(subreddit);
    }
}
