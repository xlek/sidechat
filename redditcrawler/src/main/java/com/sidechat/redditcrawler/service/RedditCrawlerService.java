package com.sidechat.redditcrawler.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Service
public class RedditCrawlerService {

    @Autowired
    private WebClient webClient;

    public String fetch() {
        return webClient.get()
                .uri("https://oauth.reddit.com/r/singaporefi/hot")
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public String crawlSubreddit(String subreddit) {
        return "";
    }
}
