package com.sidechat.indexer.kafka;


import com.sidechat.entity.Post;
import com.sidechat.indexer.repository.PostRepository;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.DltHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.retrytopic.DltStrategy;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Service;

import static org.springframework.kafka.retrytopic.TopicSuffixingStrategy.SUFFIX_WITH_INDEX_VALUE;

@Slf4j
@Service
@AllArgsConstructor
public class KafkaConsumer {

    @Autowired
    private final PostRepository postRepository;

    @WithSpan("KafkaConsumer.consume")
    @RetryableTopic(
            attempts = "2",
            backoff = @Backoff(delay = 1000, maxDelay = 100000, multiplier = 2),
            autoCreateTopics = "false",
            retryTopicSuffix = "-indexer-retry",
            dltTopicSuffix = "-indexer-dlt",
            topicSuffixingStrategy = SUFFIX_WITH_INDEX_VALUE,
            dltStrategy = DltStrategy.FAIL_ON_ERROR
    )
    @KafkaListener(topics = "post", containerFactory = "kafkaListenerContainerFactory")
    public void consume(@Payload Post post,
                        @Headers MessageHeaders messageHeaders,
                        Acknowledgment ack) {
        try {
            postRepository.save(post);
        } catch (Exception e) {
            log.error("Failed to save Post {} into elasticsearch {}", post.getId(), post);
        }
        ack.acknowledge();
    }

    @DltHandler
    public void dlt() { }


//    @KafkaListener(topics = "post")
//    public void consume(@Payload Post post) {
//        String threadId = String.valueOf(post.getThreadId());
//
//        // download and upload images into minio
//        post.getImages().forEach(image -> {
//            HttpURLConnection headMediaUrlConnection = null;
//            try {
//                headMediaUrlConnection = (HttpURLConnection) (new URL(image.getUrl())).openConnection();
//                headMediaUrlConnection.setRequestMethod("HEAD");
//                headMediaUrlConnection.connect();
//
//                int contentLength = headMediaUrlConnection.getContentLength();
//                headMediaUrlConnection.disconnect();
//
//                try (InputStream in = (new URL(image.getUrl())).openStream()) {
//                    if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(threadId).build())) {
//                        minioClient.makeBucket(MakeBucketArgs.builder().bucket(threadId).build());
//                    }
//                    minioClient.putObject(PutObjectArgs.builder()
//                            .bucket(threadId)
//                            .object(image.getTitle())
//                            .stream(in, contentLength, -1)
//                            .build());
//                } catch (ServerException | NoSuchAlgorithmException | InsufficientDataException |
//                         ErrorResponseException | InvalidKeyException | InvalidResponseException | XmlParserException |
//                         InternalException e) {
//                    log.error(e.getMessage());
//                    throw new RuntimeException(e);
//                }
//
//            } catch (IOException e) {
//                log.error(e.getMessage());
//                throw new RuntimeException(e);
//            }
//        });
//
//        // index document into elastic
//        postRepository.save(post);
//    }
}
