package com.sidechat.hwzcrawler.repository;

import com.sidechat.entity.Thread;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThreadRepository extends CrudRepository<Thread, String> {
}
