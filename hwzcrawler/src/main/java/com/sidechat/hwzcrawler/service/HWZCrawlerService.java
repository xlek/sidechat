package com.sidechat.hwzcrawler.service;

import com.sidechat.entity.Image;
import com.sidechat.entity.Post;
import com.sidechat.entity.Quote;
import com.sidechat.entity.Thread;
import com.sidechat.hwzcrawler.kafka.KafkaProducer;
import com.sidechat.hwzcrawler.repository.ThreadRepository;
import io.micrometer.core.instrument.MeterRegistry;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class HWZCrawlerService {

    @Value("${sidechat.crawler.max-thread-page}")
    private long threadPageLimit;

    @Autowired
    private MeterRegistry meterRegistry;

    Pattern threadsRegexPattern = Pattern.compile("^.*\\/threads.*[.\\/]([0-9]+)\\/");

    Random random = new Random();

    private List<String> userAgentList;

    @Autowired
    public void loadUserAgent(@Value("classpath:user-agent.txt") Resource resourceFile) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(resourceFile.getInputStream());
        userAgentList = new BufferedReader(inputStreamReader)
                .lines()
                .filter(line -> !line.isEmpty())
                .collect(Collectors.toSet()).stream().toList();
        log.info("Loaded userAgentSet with {} records", userAgentList.size());
    }

    @Value("${hwzcrawler.forum.url}")
    private String hwzForumUrl;

    @Value("${hwzcrawler.subforum.path}")
    private String hwzSubForumPath;

    @Value("${sidechat.crawler.max-thread-page}")
    private Long crawlThreadMaxPage;

    @Scheduled(cron = "0 0/60 * * * ?")
    public void hourly2Pages() {
        log.info("Running hourly job collecting top 2 pages");
        this.processTopXForumPages(2);
    }

    @Autowired
    private final ThreadRepository threadRepository;

    @Autowired
    private final KafkaProducer kafkaProducer;

    public HWZCrawlerService(ThreadRepository threadRepository, KafkaProducer kafkaProducer) {
        this.threadRepository = threadRepository;
        this.kafkaProducer = kafkaProducer;
    }

    /* Main Function, identify list of threads
       Parallelized collection of job by threads; 1 process per forumPage */
    public void processTopXForumPages(int pages) {
        // Crawl thread information within top X pages
        List<Thread> threadList = this.getAllThreadsInTopPages(pages);

        // Determine if thread has new posts
        List<Post> postList = StreamSupport.stream(threadList.spliterator(), true)
                .filter(thread -> thread.getLastPage() < threadPageLimit)
                .map(this::processThread)
                .flatMap(posts -> posts.stream())
                .collect(Collectors.toList());

        log.info("Collected {} posts", postList.size());
    }

    /* Collect thread information within top pages */
    @WithSpan
    private List<Thread> getAllThreadsInTopPages(int pages) {
        //https://forums.hardwarezone.com.sg/forums/eat-drink-man-woman.16/page-1

        List<Thread> threadList = new ArrayList<>();
        for (int i = 1; i <= pages; i++) {
            Document document = null;
            try {
                String currentPageUrl = hwzForumUrl + hwzSubForumPath + ("/page-" + i);
                document = Jsoup.connect(currentPageUrl)
                        .userAgent(userAgentList.get(random.nextInt(1000)))
                        .get();
                log.debug("Processing page #{} at {}", i, currentPageUrl);

                Elements threadsObjects = document.select(
                        "div.structItemContainer-group.js-threadList > " +
                                    "div.structItem--thread");

                for (Element t : threadsObjects) {
                    // sample1 -> /threads/<title>.<threadId>
                    // sample2 -> /threads/<threadId>
                    String threadUrl = hwzForumUrl +
                            t.select("div.structItem-title > a")
                                    .attr("href");

                    Matcher matcher = threadsRegexPattern.matcher(threadUrl);
                    matcher.matches();
                    String threadId = matcher.group(1);

                    String title = t.select("div.structItem-title").text();
                    String threadStarter = t.select(
                            "div.structItem-minor > " +
                                        "ul.structItem-parts > " +
                                        "li > a.username").text();
                    Date threadStartDate = new Date(Long.parseLong((
                                t.select("div.structItem-minor > " +
                                                "ul.structItem-parts > " +
                                                "li.structItem-startDate > a > time")
                                        .attr("data-time"))) * 1000);

                    Date threadLastModifiedDate = new Date();
                    try {
                        threadLastModifiedDate = new Date(Long.parseLong(
                                t.select("div.structItem-cell.structItem-cell--latest > a > time")
                                        .attr("data-time")) * 1000);
                    } catch (NumberFormatException e) {
                        log.warn("Unable to get thread last modified date: {}", e.getMessage());
                    }

                    // Get Approximated View Count of Thread
                    Long viewCount = this.normalizeViewCount(
                            t.select("div.structItem-cell.structItem-cell--meta > " +
                                        "dl.pairs.pairs--justified.structItem-minor > dd").text());

                    // Get Last Page of Thread
                    Elements pageJumpElement = t.select("div.structItem-minor >" +
                                                        "span.structItem-pageJump > a");
                    long lastPage = 1L;
                    if(pageJumpElement.size() > 0) {
                        try {
                            lastPage = Long.parseLong(
                                    Objects.requireNonNull(pageJumpElement.last())
                                            .attr("href")
                                            .split("page-")[1]);
                        } catch (Exception e) {
                            log.warn("Unable to get last page: {}", e.getMessage());
                        }
                    }

                    threadList.add(Thread.builder()
                            .id(threadId)
                            .url(threadUrl)
                            .title(title)
                            .creator(threadStarter)
                            .startDate(threadStartDate)
                            //.lastModified(threadLastModifiedDate)
                            //.viewCount(viewCount)
                            .lastPage(lastPage)
                            .build());
                }
            } catch (IOException e) {
                log.warn("Failed to extract thread info from page {}: {}", pages, e.getMessage());
            }
        }
        return threadList;
    }

    /* Extract individual post information */
    public Optional<Post> extractPostInfo(Element postElement, Thread thread) {
        long id = -1;
        try {
            id = Long.parseLong(postElement.attr("data-content").split("-")[1]);
            String author = postElement.attr("data-author");

            // 3 user extra info, 1. joined, 2. messages, 3. reaction
            Elements userExtraInfo = postElement.select("div.message-userExtras > dl");

            long authorPostCount = -1L;
            try {
                authorPostCount = Long.parseLong(
                        userExtraInfo.get(1)//??
                                .select("dd").text()
                                .replace(",", ""));
            } catch (Exception e) { /* ignore */ }

            Date publishedDate = new Date(Long.parseLong(
                        postElement.select("time").attr("data-time").trim()) * 1000);
            String publishedDateString = postElement.select("time").attr("title").trim();

            int localPostId = Integer.parseInt(
                    postElement
                            .select("ul.message-attribution-opposite.message-attribution-opposite--list > li")
                            .get(1).text().replaceAll("[#,]", ""));

            Element bodyElement = postElement.select("div.bbWrapper").first();

            assert bodyElement != null;
            String text = bodyElement.text();
            //String html = bodyElement.html();

            List<Image> imageList = new ArrayList<>();
            for (Element e : bodyElement.select("div.bbImageWrapper")) {
                // prepare metadata for images[]
                imageList.add(new Image(
                        e.attr("data-src"),
                        e.attr("title")
                ));
            }

            List<Quote> quoteList = new ArrayList<>();
            for (Element e : bodyElement.select("blockquote")) {
                try {
                    Element quoteHeaderElement = e.select("a.bbCodeBlock-sourceJump").first();
                    if(quoteHeaderElement == null) {
                        continue;
                    }
                    String postId = quoteHeaderElement.attr("data-content-selector").replaceAll("#post-", "");
                    String quoteAuthor = quoteHeaderElement.text().replaceAll(" said:", "");
                    String quoteText = e.select("div.bbCodeBlock-content").text()
                            .replace(quoteHeaderElement.text(), "")
                            .replace("Click to expand...", "")
                            .trim();

                    // remove quote text from text body
                    text = text
                            .replace(quoteHeaderElement.text(), "")
                            .replace(quoteText, "")
                            .replace("Click to expand...", "")
                            .trim();

                    quoteList.add(new Quote(quoteAuthor, postId, quoteText));
                } catch (Exception ignored) {
                    log.warn("Failed to extract quote element of post {} from thread {}: {}", id, thread.getId(), ignored.getMessage() );
                }
            }

            Post post = Post.builder()
                    .id(id)
                    .threadId(thread.getId())
                    .threadTitle(thread.getTitle())
                    .localPostId(localPostId)
                    .author(author)
                    .authorPostCount(authorPostCount)
                    .publishedDate(publishedDate)
                    .publishedDateString(publishedDateString)
                    .text(text)
                    //.html(html)
                    .images(imageList)
                    .quotes(quoteList)
                    .build();

            /* Send collected information to kafka */
            kafkaProducer.send(post);

            return Optional.of(post);
        } catch (Exception e) {
            log.warn("Failed to extract post element of post {} from thread {}: {}", id, thread.getId(), e.getMessage() );
            return Optional.empty();
        }
    }

    /* Parallelized collection of post within page; 1 process per post collection */
    @WithSpan
    private List<Post> processThreadPage(Thread thread, Long page) {
        List<Post> postListByPage = new ArrayList<>();

        Document document = null;
        try {
            String currentPageUrl = String.format(thread.getUrl() + "page-%d", page);
            document = Jsoup.connect(currentPageUrl)
                    .userAgent(userAgentList.get(random.nextInt(1000)))
                    .get();
            log.debug("Processing page #{}/{} of thread {}", page, thread.getLastPage(), thread.getTitle());

            Elements postsObjects = document.select(
                    "div.block-body.js-replyNewMessageContainer > " +
                            "article.message--post");

            Stream<Element> elementStream = StreamSupport.stream(postsObjects.spliterator(), false);
            elementStream
                    .map(element -> this.extractPostInfo(element, thread))
                    .filter(Optional::isPresent)
                    .forEach(post -> {
                        postListByPage.add(post.get());
                    });

        } catch (IOException e) {
            log.error("Failed to extract post info from thread {} page {}: {}", thread.getId(), page, e.getMessage() );
            e.printStackTrace();
        }
        return postListByPage;
    }

    /* Parallelized collection of thread by pages; 1 process per page
    *  Main Function for post collection of specific thread */
    @WithSpan
    public List<Post> processThread(Thread thread) {
        Optional<Thread> lastThreadRecord = threadRepository.findById(thread.getId());

        Long startingPage = lastThreadRecord.isPresent() ? lastThreadRecord.get().getLastPage() : 1;

        log.info("Processing threadId {} tasked for collection from page {} (lastCollectionRecord: {}) to {}",
                thread.getId(),
                startingPage,
                lastThreadRecord.isPresent(),
                thread.getLastPage());

        List<Post> processedPost = StreamSupport.stream(LongStream.range(startingPage, thread.getLastPage()+1).spliterator(), true)
                .map(pageNum -> this.processThreadPage(thread, pageNum))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        threadRepository.save(thread);

        return processedPost;
    }

    private Long getLongIgnoreLastChar(String str) { return Long.parseLong(str.substring(0, str.length() - 1)); }

    private Long normalizeViewCount(String numString) {
        return switch (numString.toLowerCase().charAt(numString.length()-1)) {
            case 'k' -> this.getLongIgnoreLastChar(numString) * 1000;
            case 'm' -> this.getLongIgnoreLastChar(numString) * 1000000;
            default -> {
                try {
                    yield Long.parseLong(numString.replaceAll("[^0-9]", ""));
                } catch (Exception pe) {
                    pe.printStackTrace();
                    yield  0L;
                }
            }
        };
    }

    public Optional<Thread> getThreadInfo(String threadId) {
        //https://forums.hardwarezone.com.sg/threads/this-meow-meow-is-a-professional-lol.6832604/
        //https://forums.hardwarezone.com.sg/threads/6832604/page-1

        String threadUrl = hwzForumUrl + String.format("/threads/%s/", threadId);

        Document document = null;
        try {
            document = Jsoup.connect(threadUrl)
                    .userAgent(userAgentList.get(random.nextInt(1000)))
                    .get();

            String title = document.select("h1.p-title-value").text();
            String creator = document.select("div.p-description > ul > li > a.username").text();
            Date threadCreatedDate = new Date(Long.parseLong(
                    document.select("div.p-description > ul > li > a > time")
                            .attr("data-time")) * 1000);

            Elements pageNav = document.select("div.pageNav > ul.pageNav-main > li");
            long lastPage = 1;
            if(!pageNav.isEmpty()) {
                lastPage = Long.parseLong(Objects.requireNonNull(pageNav.last()).text());
            }

            return Optional.of(Thread.builder()
                    .id(threadId)
                    .url(threadUrl)
                    .title(title)
                    .creator(creator)
                    .startDate(threadCreatedDate)
                    .lastPage(lastPage)
                    .build());

        } catch (Exception e) {
            log.error("Failed to get thread info from thread id {}: {}", threadId, e.getMessage());
            return Optional.empty();
        }
    }
}
