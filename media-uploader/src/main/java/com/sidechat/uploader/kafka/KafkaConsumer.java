package com.sidechat.uploader.kafka;

import com.sidechat.entity.Post;
import com.sidechat.uploader.service.UploaderService;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.retrytopic.DltStrategy;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.kafka.retrytopic.TopicSuffixingStrategy.SUFFIX_WITH_INDEX_VALUE;


@Slf4j
@Service
@AllArgsConstructor
public class KafkaConsumer {
    @Autowired
    private final UploaderService uploaderService;

    @WithSpan("KafkaConsumer.consume")
    @RetryableTopic(
            attempts = "2",
            backoff = @Backoff(delay = 1000, maxDelay = 100000, multiplier = 2),
            autoCreateTopics = "false",
            retryTopicSuffix = "-uploader-retry",
            dltTopicSuffix = "-uploader-dlt",
            topicSuffixingStrategy = SUFFIX_WITH_INDEX_VALUE,
            dltStrategy = DltStrategy.FAIL_ON_ERROR
    )
    @KafkaListener(topics = "post")
    public void consume(@Payload List<Post> postList,
                        @Headers MessageHeaders messageHeaders,
                        Acknowledgment ack) {
        postList.forEach(post -> {
            if (post.getImages().size() > 0) {
                try {
                    uploaderService.processUpload(post);
                } catch (Exception e) {
                    log.error("Failed to process post {} with media url: {}", post.getId(), e.getMessage());
                }
            }
        });
        ack.acknowledge();
    }
}
