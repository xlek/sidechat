package com.sidechat.uploader.service;

import com.sidechat.entity.Post;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@Slf4j
@Service
@AllArgsConstructor
public class UploaderService {

    @Autowired
    private final MinioClient minioClient;

    public void processUpload(Post post) {

        String threadId = String.valueOf(post.getThreadId());

        // download and upload images into minio
        post.getImages().forEach(image -> {
            HttpURLConnection conn = null;

            int contentLength = 0;
            try {
                conn = (HttpURLConnection) (new URL(image.getUrl())).openConnection();
                conn.setConnectTimeout(5000);
                conn.setReadTimeout(5000);
                conn.connect();

                contentLength = conn.getContentLength();
                InputStream in = conn.getInputStream();

                if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(threadId).build())) {
                    minioClient.makeBucket(MakeBucketArgs.builder().bucket(threadId).build());
                }
                minioClient.putObject(PutObjectArgs.builder()
                        .bucket(threadId)
                        .contentType(conn.getContentType())
                        .object(image.getTitle())
                        .stream(in, contentLength, -1)
                        .build()
                );

                log.info("Successfully download media url {} into MinIO /{} bucket",
                        image.getUrl(),
                        threadId);

            } catch (Exception e) {
                throw new RuntimeException(String.format("[%s]: Failed to download media url %s into MinIO",
                        e.getClass().getSimpleName(),
                        image.getUrl()
                ));
            } finally {
                assert conn != null;
                conn.disconnect();
            }
        });
    }
}
